import Testing

print "Q6 Test"
print "Pen Data Tests\n"
i = 0
step = 5
while i <= 40:
    accuracy = []
    for x in range(5):
        net, currentAcc = Testing.testPenData(hiddenLayers=[i])
        accuracy.append(currentAcc)
    print "Pen data for", i, "perceptrons"
    print "Pen data max accuracy", max(accuracy)
    print "Pen data average accuracy:", Testing.average(accuracy)
    print "Pen data standard deviation", Testing.stDeviation(accuracy)
    print "\n"
    i += step

print "Car Data Tests\n"
i = 0
while i <= 40:
    accuracy = []
    for y in range(5):
        net, currentAcc = Testing.testCarData(hiddenLayers=[i])
        accuracy.append(currentAcc)
    print "Car data for", i, "perceptrons"
    print "Car data max accuracy", max(accuracy)
    print "Car data average accuracy:", Testing.average(accuracy)
    print "Car data standard deviation", Testing.stDeviation(accuracy)
    print "\n"
    i += step
