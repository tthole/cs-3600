import Testing

print "Q5 Test"
accuracy = []
for i in range(5):
    nnet, testAccuracy = Testing.testPenData()
    accuracy.append(testAccuracy)
print "Pen data max accuracy", max(accuracy)
print "Pen data average accuracy:", Testing.average(accuracy)
print "Pen data standard deviation", Testing.stDeviation(accuracy)

accuracy = []
for i in range(5):
    nnet, testAccuracy = Testing.testCarData()
    accuracy.append(testAccuracy)
print "Car data max accuracy", max(accuracy)
print "Car data average accuracy:", Testing.average(accuracy)
print "Car data standard deviation", Testing.stDeviation(accuracy)